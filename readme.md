# Cross Compiling

## Beagleboneblack
[Reference](https://github.com/Leonti/rust-socket-server)

1. `sudo apt-get install libudev-dev`
2. create `~/.cargo/config` and add 
```bash
[target.armv7-unknown-linux-musleabihf]
linker = "arm-linux-gnueabihf-gcc-8"
```
3. Add target and install gcc toolchain
```bash
rustup target add armv7-unknown-linux-musleabihf
sudo apt-get install gcc-8-multilib-arm-linux-gnueabihf
```
4. Compile Project
`cargo build --target=armv7-unknown-linux-musleabihf --release`