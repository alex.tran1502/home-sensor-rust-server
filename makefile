build:
	cross build --target armv7-unknown-linux-musleabihf --release

install:
	scp target/armv7-unknown-linux-musleabihf/release/home_wellness_server debian@192.168.7.2:/home/debian
	scp .env debian@192.168.7.2:/home/debian