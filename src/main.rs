extern crate chrono;
extern crate serde;

use serde::Deserialize;
use serde_json::Deserializer;

use std::env;
use dotenv::dotenv;
use sqlx::{postgres::PgPool};
use chrono::{Utc};
mod sensors;
#[derive(Deserialize, Debug, Default)]
pub struct SensorPacket {
    node: String,
    light: f64,
    object_temp: f64,
    ambient_temp: f64,
    humidity: f64,
    pressure: f64,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {

    let conn = establish_db_connection().await?;
    let devices = sqlx::query!("SELECT * FROM sensor_node").fetch_all(&conn).await?;
    println!("Database connection {:#?}", devices);
    let mut sensor_port = sensors::get_port().unwrap();
    
    loop {
        let stream = Deserializer::from_reader(&mut sensor_port).into_iter::<SensorPacket>();

        let sensor_packet: Option<SensorPacket> = stream.filter_map(|packet| packet.ok()).next();

        if !sensor_packet.is_some() {
            continue;
        }

        let packet: SensorPacket = sensor_packet.unwrap_or_default();
        let record = sqlx::query!(r#"
                INSERT INTO public.records
                ("timestamp", node, light, object_temp, ambient_temp, humidity, pressure)
                VALUES($1, $2, $3, $4, $5, $6, $7);
                "#,
            Utc::now(),
            packet.node,
            packet.light,
            packet.object_temp,
            packet.ambient_temp,
            packet.humidity,
            packet.pressure
        ).execute(&conn).await?;
        
        println!("Execute Query {:?} at {}", record, Utc::now());
    }
}


async fn establish_db_connection() -> Result<PgPool, sqlx::Error>{
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let connection = PgPool::builder().max_size(5).build(&database_url).await?;

    return Ok(connection);
}



fn process_sensor_packet(packet: SensorPacket) {
    println!("{:#?}", packet);
}
