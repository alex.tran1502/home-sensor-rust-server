use serialport::prelude::*;
use std::time::Duration;

pub fn get_port() -> Result<Box<dyn serialport::SerialPort>, serialport::Error> {
    // let port_name: String = get_available_port();
    let port_name = "/dev/ttyACM0";
    println!("Opening Port {}...", port_name);

    let s = SerialPortSettings {
        baud_rate: 115_200,
        data_bits: DataBits::Eight,
        flow_control: FlowControl::None,
        parity: Parity::None,
        stop_bits: StopBits::One,
        timeout: Duration::from_millis(1),
    };

    let port = serialport::open_with_settings(&port_name, &s)?;

    Ok(port)
}

fn get_available_port() -> String {
    let mut ti_port: Vec<String> = Vec::new();
    let available_ports = serialport::available_ports().unwrap();

    for port in available_ports.iter() {

        match &port.port_type {
            serialport::SerialPortType::UsbPort(info) => {
                let manufacturer = info.manufacturer.as_ref().map_or("", String::as_str);
                if manufacturer == "Texas Instruments" || manufacturer == "Texas_Instruments" || manufacturer == "Texas Instruments Incorporated" {
                    ti_port.push(port.port_name.to_string());
                }
            },
            _ => ()
        }
    }


    #[cfg(target_os = "macos")]
    return ti_port[0].to_string();
    #[cfg(target_os = "linux")]
    return ti_port[0].to_string();
    #[cfg(target_os = "windows")]
    return ti_port[1].to_string();
    
}
